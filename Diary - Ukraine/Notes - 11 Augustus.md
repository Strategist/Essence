Notes 11 Augustus

Soms gaan er werelden voor je open op plekken waar je ze niet verwacht. Vanavond zijn we naar een soort open lucht concert op een Roma kamp geweest. De muziek die ze maakten was echt mooi, het deed me verlangen naar een wereld met een mooi verhaal, een wereld van liefde en avontuur. Ik merk dat het verlangen naar zo'n wereld bij mij steeds sterker wordt. Ik verlang naar avontuur, of beter gezegd: Naar beweging in mijn leven. Niet zo zeer het verlangen naar een kick uit ervaringen maar meer de verzadiging van het beter kunnen begrijpen van het leven. Na zulke avonden met muziek die mijn verlangens raakt raak ik ervan overtuigd dat er meer is dan alleen het fysieke. Iets in mij weet dan dat het niet alleen een verzameling van stoffen en atomen is maar dat er een reden is dat wij mensen zo'n soort nostalgie kunnen voelen. Een verlangen of het voelen van een wereld of een verhaal waar we zelf in zitten maakt ons mensen tot iets bijzonders.

Een oprechte lach van een kind bewijst iets, ik weet alleen nog niet wat. Dat het leven echt puur en oprecht kan zijn?

Versnelling (Sometimes emotions slow shift to higher or lower levels)
Fire Distance (Something on a small distance can give warmth, but on a close distance burn you.)

Silence can be more interesting then noisy companions.

Silence sometimes says more then we think it does

I guess I like silence because it creates the ability to think. 
I love thoughts even though my mind is generally chaos most of the time.
I hope to figure it out one day :)
# 1 Thessalonians

*Estimated time of writing:* 52 CE
*Oldest fragment found: * 175~225 CE (https://en.wikipedia.org/wiki/Papyrus_46)
*Dating Technique: * Palaeography only

## General information
The first letter to the Thessalonians. Probably written in 52 AD, making it the oldest book in the New Testament.

- Most scholars believe it was written in Corinth although information appended to early manuscript suggest that Paul wrote it in Athens after Timothy had returned from Macadonia with the news of the state of the church at Thessalonica. (Acts 18:1–5; 1 Thes. 3:6)

1 Thessalonians 2:13–16 have often been regarded as a post-Pauline interpolation. The following arguments have been based on the content:

    It is perceived to be theologically incompatible with Paul's other epistles: elsewhere Paul attributed Jesus's death to the "rulers of this age" (1 Cor 2:8) rather than to the Jews, and elsewhere Paul writes that the Jews have not been abandoned by God, for "all Israel will be saved" (Rom 11:26); According to 1 Thes 1:10, the wrath of God is still to come, it is not something that has already shown itself.[6]
    There were no extensive historical persecutions of Christians by Jews in Palestine prior to the first Jewish war.[7]
    The use of the concept of imitation in 1 Thes. 2.14 is singular.
    The aorist eftasen ("has overtaken") refers to the destruction of Jerusalem.[8]
    The syntax of 1 Thes. 2:13–16 deviates significantly from that of the surrounding context.[9]

It is also sometimes suggested that 1 Thes. 5:1–11 is a post-Pauline insertion that has many features of Lukan language and theology that serves as an apologetic correction to Paul's imminent expectation of the Second Coming in 1 Thes. 4:13–18.[10]

## Eigen vertaling aan de hand van de Griekse grondtekst

1. Paul and Silvanus and Timothy to the church of the Thessalonians,
   In God the Father and the Lord Jezus Christ: Grace to you and peace, from God our Father and the Lord Jezus Christ.

- Wat betekent: "In God the Father ..."

2. We thank God concerning all of you, mentioning/remembering you in our prayers,

- Waarom danken de schrijvers God voor hen?
- Waarom heeft het zin om de kerk in Thessalonians bij name te noemen in gebed?

3. Unceasingly remembering your work of (faith, belief, trust, confidence, fidelity, faithfulness, loyalty, commitment, and proof.) and your labor of (love, good will, benevolence, charity) and the (endurance, steadfastness) of (hope, expectation of what is certain, anticipation) of our Lord, Jesus Christ, before God and Father of us.

4. Knowing, brothers beloved by God, the election (selection out of and to a given outcome) of you.

Waarom


# Datering evangelien

## Lucas
De datering van het Lucasevangelie wordt meestal na 70 gesteld. De reden hiervan is dat Lucas in zijn weergave van de apocalyptische rede van Jezus veel concreter is dan de evangelisten Mattheüs en Marcus. Vanuit de veronderstelling dat Lucas het Marcus-evangelie kende, en als basis voor zijn weergave heeft gebruikt, komen geleerden tot de gedachte dat Lucas de apocalyptische rede van Jezus concreet heeft toegepast op de verwoesting van Jeruzalem in 70 na Christus. 

J.M.M Thurlings verkent in dit artikel een andere verklaring. Volgens Thurlings zijn er bij de schrijver die wij Lucas noemen sporen te zien dat hij soms informanten verkeerd verstond. Vanuit zijn Griekse achtergrond zou Lucas zijn bronnen, wanneer ze aan hem voorgelezen werden, wel eens anders kunnen hebben geinterpreteerd. Thurlings geeft hier in zijn artikelen enkele voorbeelden van. Verschillende voorbeelden zijn juist te vinden in de apocalyptische rede van Jezus (!). 

Zo komt Thurlings tot de conclusie dat Lukas de apocalyptische rede van Jezus wel eens onbedoeld van te voren meer richting de concrete verwoesting van Jeruzalem zou kunnen hebben beschreven. Dit maakt het mogelijk dat het Lucasevangelie vroeger gedateerd wordt dan 70 na Christus. 


Echte brieven van Paulus:

-> 1 Thessalonians - 51 AC
-> Philippians - 54/55 AC
-> Philemon - 54/55 AC
-> Galatians - 55 AC
-> Corinthians - 56 AC
-> Romeinen - 57 AC
-> Colossians - 62/70 AC (some suggest 55 AC)

Waarschijnlijk neppe brieven:

- Ephesians - 80/90 Na Christus     (Letters seems to be written after Pauls death in Rome, by an author who uses his name.)
- 2 Thessalonians - 51 AC (Or post 70 AC) -> Might be fake.
- Hebrews - 80/90 AC
(Hebrews 	c. 80–90 CE. The elegance of the Greek and the sophistication of the theology do not fit the genuine Pauline epistles, but the mention of Timothy in the conclusion led to its being included with the Pauline group from an early date.[71])

Anders:

1 Timothy - 100 AC
2 Timothy - 100 AC
Titus - 100 AC
James - c. 65–85 CE. Like Hebrews, James is not so much a letter as an exhortation; the style of the Greek makes it unlikely that it was actually written by James the brother of Jesus.[71]
Petrus 1 - 75/90 AC
Petrus 2 - 110 CE
Johannes - 90–110 CE.[77] The letters give no clear indication, but scholars tend to place them about a decade after the Gospel of John.[77]
Jude - Uncertain. The references to "brother of James" and to "what the apostles of our Lord Jesus Christ foretold" suggest that it was written after the apostolic letters were in circulation, but before 2 Peter, which uses it.[71]
Relevation - c. 95 CE. The date is suggested by clues in the visions pointing to the reign of the emperor Domitian.[71]

# Questions about 1 Thessalonians
Preinformation:
Url: https://en.wikipedia.org/wiki/First_Epistle_to_the_Thessalonians#Authenticity

1 Thessalonians 2:13-16 is likely not to have been written by the original author.
- Different Syntax
- Different vision

1 Thes. 5:1–11 is also suggested not to have been written by the original author.


General:
- What was the date of writing?
- What was the location of writing?
- What was the intention of writing?
- Why did Paul write letters?
- Did Paul more often write letters?
- Did he write more letters to Thessalonians (how many?)
- How to he settle the church in Thessalonians?
- What kind of people where Thessalonians?

______
## Chapter 1
Verse 1:
- Paul, Silvanus en Timothy were probably together at the time of writing.
- The church of the Thessalonians was already founded at the time of writing.
- Jezus Christ is called "Lord".
- God is called Father.

Verse 2:
- Paul thanks God for the church in Thessalonians.

Verse 3:
- Paul exeggarates a bit by using "unceasingly".
- The Thessalonians do "Work of faith" and "Labor of love"
- **What kind of labor and work?**
- The had endurance in the hope of Lord Jezus Christ.

Verse 4:
- **God chooses the Thessalonians? For what? What does this mean?**

Verse 5: 
- **The gospel does not come in word only but also in power, and in the Holy Spirit with full conviction.**
- 
- Paul, Silvanus and Timoty state that they proved "what kind of men" they were.

Verse 6:
- The Thessalonians became imitators of Paul, Silvanus and Timothy and of the Lord.
- They received the word in tribulations with joy of the Holy Spirit.

Verse 7:
- The Thessalonians were an example to all the believers in Macedonia and Achaia.
     - **Where lies Macedonia and Achaia?**
	 
Verse 8:
- The word of the Lord sounded everywhere, at all places they came to be.
- Paul, Silvanus and Timothy do not have to say anything to improve the situation.

Verse 9:
- The deeds report what reception Paul, Silvanus and Timothy had with them.
- The Thessalonians first had false idols.

Verse 10:
- God has raised Jezus from the dead
- Jezus is in heaven.
- The Thessalonians must wait for His Son from heaven.
- Jezus will rescue us from the (wrath / passion / punishment) to come.

__________
## Chapter 2

Verse 1:
- **Paul, Timothy and Silvanus have been in Thessalonians.**

Verse 2:
- **They had earlier been mistreated in Philippi.**
- They spoke about the gospel amids the opposition.

Verse 16: 
- hindering us from speaking to the Gentiles so that they may be saved; with the result that they always fill up the measure of their sins. But wrath has come upon them [t]to the utmost.

Verse 18:
- more than once—and yet Satan hindered us.
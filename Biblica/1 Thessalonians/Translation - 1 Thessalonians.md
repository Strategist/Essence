# First Epistle to the Thessalonians
*Estimated date of writing*: 52 AD
*Oldest version found*: Papyrus 46 (175 ~ 225 AD)

## Introduction

## Dating and Authenticity
Most scolars believe that this letter is written by Paul to the Thessalonian church that has been founded earlier (When?) by Paul. The 

## History about the Thessalonians
Thessalonian (also called called Thessaloniki) is a city in Greece (https://en.wikipedia.org/wiki/Thessaloniki)

## Translation

### Chapter 1
*Sentence*: `Ɩ?婭, 䖿 ϣ婲֧?�䖿 У梢ީ�?䤥ᬟ? ̞쬖婧㤝ꧠ?砬޿ Ɩ 䖿 ղ럿 ?ᬩ? ӫ㬮?: 䛫㭠?濧 䖿 ޿랧ᠿ?꿠➩? ꖮ뿭 ?濧 䖿 䲫ߩ⠿ᬩ? 䫣쮩??.`
| Greek Word | Prononciation | Word Type | Translation
|-------|----------------|-----------|-------------
`Ɩ?婭` | Paulos | Noun - Nominative Masculine Singular | Paul
`䖿` | Kai | Conjunction | and, even, also, namely
`ϣ婲֧?�| Silouanos | Noun - Nominative Masculine Singular | Silvanus
`䖿` | Kai | Conjunction | and, even, also, namely
`У梢ީ�| Timotheos | Noun - Nominative Masculine Singular | Timothy
` | Te | Article - Dative Feminine Singular | to the
`?䤥ᬟ?`| Ekklesia | Noun - Dative Feminine Singular | church
`̞쬖婧㤝ꧠ | Thessalonikeon | Noun - Genitive Masculine Plural | Thessalonians
`?砠| En | Preposition | in, on, among
`̞?` | Theo | Noun - Dative Masculine Singular | God
`Ɩ` | Patri | Noun - Dative Masculine Singular | Father, Ancestor
`䖿` | Kai | Conjunction | and, even, also, namely
`ղ럿` | Kyrio | Noun - Dative Masculine Singular | Lord, Master, Sir
`?ᬩ?` | Iesou | Noun - Dative Masculine Singular | Jezus
`ӫ㬮?` | Christo | Noun - Dative Masculine Singular | Christus
`䛫㭠 | Charis | Noun - Nominative Feminine Singular | Grace
`?濧` | hymin | Personal/Possesive Pronoun - Dative 2d Person Plural | You
`䖿` | Kai | Conjunction | and, even, also, namely
`޿랧ᠠ| Eirene | Noun - Nominative Feminine Singular | Peace, Peace of mind
`??꿠 | Apo | Preposition | from, away from
`➩?` | Theou | Noun - Genitive Masculine Singular | God
`ꖮ뿭` | Patros | Father | Noun - Genitive Masculine Singular | Father
`?濧` | Hemon | Personal/Possesive Pronoun - Genitative 1st Person Plural | Our
`䖿` | Kai | Conjunction | and, even, also, namely
`䲫ߩ⠠| Kyriou | Noun - Genitive Masculine Singular | Lord
`?ᬩ?` | Iesou | Noun - Genitive Masculine Singular | Jezus
`䫣쮩??` | Christou | Noun - Genitive Masculine Singular | Christ

**New American Standard**:
"Paul and Silvanus and Timothy,
To the church of the Thessalonians in God the Father and the Lord Jesus Christ: Grace to you and peace."

**My Translation**: 
"Paul and Silvanus and Timothy,
To the church of the Thessalonians in God, Father and Lord Jesus Christ: Grace to you and peace from God our Father and Lord Jezus Christ."

Een brief van hart tot hart en ziel tot ziel,

Ik schrijf nu omdat ik je wil uitleggen waarom ik ben wie ik ben en waarom ik de dingen die ik denk en geloof geloof.

# Wijsheid
Toen ik in de eerste van de middelbare school zat had ik als droom om wijs te worden. In films zoals Harry Potter of Lord of the Rings zag ik dan van die oude wijze mannen met lange witte baarden die op het juiste moment de juiste beslissingen namen en altijd op een mooie manier de boodschap die iemand nodig had konden verwoorden. Na een korte tijd bedacht ik me dat het toen vooral het aanzien was waar ik naar verlangde. Toch wilde ik wel dat aanzien bereiken door het doen van goede dingen i.p.v aanzien in het algemeen. Zo heb ik b.v.b. nooit de drang gehad om in aanzien te staan doordat ik veel macht bezat of iets in de geest.

Toch wilde ik me meer verdiepen in de wijsheid waar ik zo naar op zoek was. Immers geloofde ik dat ik door wijsheid te bemachtigen iemand van waarde kon worden. In de eerste van de middelbare school was ik ook iemand die echt in aanzien stond bij mijn klasgenoten. Ik haalde goede cijfers zonder arrogant te zijn en ik was vriendelijk tegen iedereen (dat blijft altijd raar om van jezelf te zeggen, maar ik vertrouw erop dat de mensen die tegen mij zeiden dat ik vriendelijk was dat ook echt meenden). Veel mensen vertelden mij hun geheimen en ze vertrouwden erop dat ik ze niet zou door vertellen. Toch kwam er na een tijdje rond de 3de een punt dat een aantal jongens de behoefte hadden zich wat dominanter op te stellen en stond ik voor de keuze om daarin mee te gaan en het aanzien wat ik in de eerste en tweede van mensen genoot op een andere manier kon bemachtigen, of om te blijven bij de wijsheid die ik zo adoreerde, waardoor je minder op de voorgrond stond en misschien minder opviel.

Dat moment heeft achteraf gezien mij misschien wel het meeste aan het nadenken gezet tot de vraag: "Wie ben ik?" of misschien beter "Wie wil ik zijn?". Uiteindelijk heb ik voor het laatste gekozen omdat ik niet mezelf wil verloochenen door iemand te zijn die ik niet ben, en zou ik al helemaal niet in aanzien willen staan via de verkeerde wegen.

Ik denk dat de momenten waarop je een keuze moet maken tussen een imago hooghouden en iets doen omdat je denkt dat het goed is misschien wel de meest cruciale momenten zijn waarop je ontdekt wie je bent.

__________________

Ik kan me nog herinneren dat ik gesprekken had met mijn vader over wat wijsheid precies inhield. We waren het er allebei over eens dat alleen "levens-ervaring" niet voldoende is om echt wijs te zijn. Immers is niet iedereen die 80 is wijs toch? Ook denk ik dat het niet alleen te maken heeft met kennis. Tenslotte kan iemand die super veel weet, maar een hele massa moord begint nooit wijs zijn toch? Of denk je dat je ook wijs kan zijn zonder dat je doet wat je weet dat goed is?

Voor mij is wijsheid: De kunst van het goede doen.

Which brings us to the next question :) :)

## Wat is goed?
Een interessante vraag al zeg ik het zelf waar de mensheid zich waarschijnlijk al hun hele bestaan mee bezig houd: Wat is goed? De eerste vraag die daaraan vooraf gaat is denk ik: "Zijn goed en slecht absolute waarden?" Daarmee bedoel ik: Is er 1 iets of iemand wat bepaald wat goed en slecht is? Of mag iedereen dat voor zichzelf bepalen en is het iets wat we zelf 'uitgevonden' hebben? Als iedereen namelijk voor zichzelf kan bepalen wat goed en slecht is dan is het onmogelijk om een zinnige uitspraak te doen over goed en kwaad. Immers kan een zelfmoord terrorist het best goed vinden dat hij zichzelf opoffert om een aantal mensen met hem mee te nemen en als iedereen voor zichzelf bepaalt wat goed en slecht is, kan het ook best lastig zijn om iemand te overtuigen. 

Zelf denk ik dat de maker van iets bepaald wat goed is en niet. Als ik een schaar ontwerpt die niet kan knippen dan is de schaar "slecht" omdat de schaar zijn doel niet vervuld. Natuurlijk kan de schaar wel opeens brilliant zijn om mee te zagen b.v.b. maar voor het doel "knippen" is de schaar niet geschikt. Ik geloof dat het zo ook is met mensen. Ik geloof dat God bepaalt wat goed is en wat slecht. Ik denk dat Hij ons met een bepaald doel heeft gemaakt en dat alles wat bijdraagt aan dat doel goed is en alles wat dat doel tegenwerkt verkeerd is.

Nu vloeien hier natuurlijk nog een aantal vragen voort zoals: "Met wat voor doel heeft God ons gemaakt?" of wat ik denk dat daar bijna aan gelijk is: "Wat is goed en wat niet?" In de Bijbel staan er verschillende hints over de reden waarom God ons heeft gemaakt en met logisch nadenken kan je ook al een heel eind komen. Neem bijvoorbeeld de volgende teksten:

Handelingen 17:24-25 - God heeft niets nodig van ons dus God kan ons ook niet gemaakt hebben met de reden dat Hij eenzaam zou zijn of iets van ons nodig zou hebben.

Genesis 1:27 - God heeft de mens gemaakt naar zijn beeld. Ook wordt hier gezegd dat mensen de macht krijgen over de dieren op de aarde.

Kollossenzen 1:16 - "All dingen zijn gemaakt door Hem en voor Hem."

1 Johannes 4:8 - Ik denk dat simpele zinnetje: "God is liefde" misschien nog wel het meeste antwoord geeft op de vraag. Ik geloof dat echte liefde iets is wat zich wil vermenigvuldigen. Echte liefde is aanstekelijk denk ik en liefde verlangt er naar om weer gedeelt te worden met anderen. Ik geloof dat God ons heeft gemaakt om lief te hebben, de belangrijkste geboden: Heb God lief boven alles en je naaste lief als jezelf geven hier al een hint van. Andere teksten als dat we voor de aarde mogen zorgen of tot Zijn eer mogen leven vloeien daaruit voort denk ik.

Dus als God ons heeft gemaakt met het doel om elkaar lief te hebben dan kunnen we eindelijk de vraag beantwoorden: Wat is goed? Ik geloof dat alles wat bijdraagt aan de liefde voor God en de liefde voor elkaar goed is en dat alles wat daar tegenin gaat verkeerd is. Dus als je jezelf ooit eens afvraagt: Wat is goed om te doen? Vraag jezelf dan: Wat is liefdevol om te doen? Dan denk ik je al op het goede spoor zit.
